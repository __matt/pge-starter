#pragma once
#include <gl/glew.h>
#include <SDL.h>
#include <iostream>
#include <assert.h>
#include "ModelRenderer.h"


class GameEngine {
public:
	GameEngine(bool vsync = true);
	GameEngine(int width, int height, bool vsync = true);
	virtual ~GameEngine();

	void update();
	void render();
	bool keepRunning();

private:
	const int windowWidth, windowHeight, windowFlags;
	bool vSync;
	SDL_Window* window;
	SDL_GLContext glContext;

	bool init();
	void createSceneObjects();
	void destroySeneObjects();

	//Scene Objects
	ModelRenderer* modelRenderer;
};