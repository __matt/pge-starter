#include "GameEngine.h"


int main(int argc, char* argv[]) {
	GameEngine* engine = new GameEngine();

	while (engine->keepRunning()) {
		engine->update();
		engine->render();
	}

	delete engine;
	return 0;
}