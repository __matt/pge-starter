#pragma once

#include <glm/glm.hpp>
#include <SDL.h>
#include <glm/gtc/matrix_transform.hpp>


class Camera
{
public:
	Camera(SDL_Window* window)
	{
		float fov = 45.0f;
		float aspectRatio = 640.0f / 480.0f;
		float near = 0.1f;
		float far = 900.0f;

		this->position = glm::vec3(0.0f, 0.0f, -5.0f);
		this->lookAt = glm::vec3(0.0f, 0.0f, 1.0f);

		projectionMatrix = glm::perspective(fov, aspectRatio, near, far);
		updateViewMatrix();
	}

	virtual ~Camera() = default;

	glm::mat4 getViewMatrix() { return viewMatrix; }
	glm::mat4 getProjectionMatrix() { return projectionMatrix; }
	glm::vec3 getPosition() { return position; }
	glm::vec3 getLookAt() { return lookAt; }

	void setPosition(float x, float y, float z) { this->position = glm::vec3(x, y, z); }
	
	void updateViewMatrix() { viewMatrix = glm::lookAt(position, lookAt, glm::vec3(0.0f, 1.0f, 0.0f)); }
	
	virtual void update() {}
	virtual void handleInput(SDL_Event&) {}

protected:
	SDL_Window* window;
	glm::mat4 viewMatrix;
	glm::mat4 projectionMatrix;

	glm::vec3 position;
	glm::vec3 lookAt;
};