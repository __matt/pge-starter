#include <gl/glew.h>
#include <vector>

struct Vertex {
	Vertex(float xPos, float yPos, float zPos, float uCoord, float vCoord) {
		x = xPos;
		y = yPos;
		z = zPos;
		u = uCoord;
		v = vCoord;
	}
	float x, y, z, u, v;
};

namespace RenderUtils {
	unsigned int createVertexBuffer(const std::vector<Vertex>& vertices);
}