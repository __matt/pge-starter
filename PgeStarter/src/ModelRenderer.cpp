#include "ModelRenderer.h"

ModelRenderer::ModelRenderer()
{
	std::string vertexShaderSource =
		"Your\n"
		"Vertex\n"
		"Shader\n"
		"Here\n";
	std::string fragShaderSource = 
		"Your\n"
		"Fragment\n"
		"Shader\n"
		"Here\n";
	programId = glCreateProgram();
	// Vertex Shader
	unsigned int vertexShader = glCreateShader(GL_VERTEX_SHADER);
	char* source = (char*)vertexShaderSource.c_str();
	glShaderSource(vertexShader, 1, &source, nullptr);
	glCompileShader(vertexShader);
	int status = GL_FALSE;
	glGetShaderiv(vertexShader, GL_COMPILE_STATUS, &status);
	if (status != GL_TRUE) {
		std::cerr << "Could not compile vertex shader!" << std::endl;
	}

	// Fragment Shader
	unsigned int fragShader = glCreateShader(GL_FRAGMENT_SHADER);
	source = (char*)fragShaderSource.c_str();
	glShaderSource(fragShader, 1, &source, nullptr);
	glCompileShader(fragShader);
	status = GL_FALSE;
	glGetShaderiv(fragShader, GL_COMPILE_STATUS, &status);
	if (status != GL_TRUE) {
		std::cerr << "Could not compile vertex shader!" << std::endl;
	}

	glLinkProgram(programId);
	glGetProgramiv(programId, GL_LINK_STATUS, &status);
	if (status != GL_TRUE) {
		std::cerr << "Could not lnik shaders!" << std::endl;
	}
	glValidateProgram(programId);
	glGetProgramiv(programId, GL_VALIDATE_STATUS, &status);
	if (status != GL_TRUE) {
		std::cerr << "Could not lnik shaders!" << std::endl;
	}

	glDeleteShader(vertexShader);
	glDeleteShader(vertexShader);
}

