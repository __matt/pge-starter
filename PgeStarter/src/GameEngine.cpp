#include "GameEngine.h"

GameEngine::GameEngine(bool vs)
	: windowWidth(1280)
	, windowHeight(1024)
	, windowFlags(SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN)
	, window(nullptr)
	, glContext(nullptr)
	, vSync(vs)
{
	if (!init()) {
		std::cout << "Could not initialise engine... Check console output." << std::endl;
		return;
	}
	createSceneObjects();
}

GameEngine::GameEngine(int width, int height, bool vs)
	: windowWidth(width)
	, windowHeight(height)
	, windowFlags(SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN)
	, window(nullptr)
	, glContext(nullptr)
	, vSync(vs)
{
	if (!init()) {
		std::cout << "Could not initialise engine... Check console output." << std::endl;
		return;
	}
	createSceneObjects();
}

bool GameEngine::init() {
	if (SDL_Init(SDL_INIT_VIDEO) < 0) {
		std::cerr << "[SDL Error] Unable to init SDL! " << SDL_GetError() << std::endl;
		return false;
	}
	// Set OpenGL version: I chose 4.5 core here.
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 4);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 5);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
	
	window = SDL_CreateWindow(":D", 50, 50, windowWidth, windowHeight, windowFlags);
	assert(window);
	glContext = SDL_GL_CreateContext(window);
	assert(glContext);
	
	GLenum status = glewInit();
	if (status != GLEW_OK) {
		std::cerr << "[GLEW Error]: " << glewGetErrorString(status) << std::endl;
		return false;
	}
	if (vSync) {
		if (SDL_GL_SetSwapInterval(1) != 0) {
			std::cerr << "[Error] Unable to set VSync! " << SDL_GetError() << std::endl;
			return false;
		}
	}

	return true;
}

GameEngine::~GameEngine() {
	destroySeneObjects();
	SDL_GL_DeleteContext(glContext);
	SDL_DestroyWindow(window);
	SDL_Quit();
}

void GameEngine::update()
{
}

void GameEngine::render()
{
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

	glClear(GL_COLOR_BUFFER_BIT);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LESS);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	//glEnable(GL_CULL_FACE);


	SDL_GL_SwapWindow(window);
}

bool GameEngine::keepRunning()
{
	SDL_PumpEvents();
	SDL_Event evt;
	if (SDL_PeepEvents(&evt, 1, SDL_GETEVENT, SDL_QUIT, SDL_QUIT))
		return false;

	return true;
}


void GameEngine::createSceneObjects() {
	//modelRenderer = new ModelRenderer();
	

}

void GameEngine::destroySeneObjects()
{
	if (modelRenderer) delete modelRenderer;
}

