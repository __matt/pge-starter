#include <gl/glew.h>
#include <iostream>
#include <string>


class ModelRenderer {
public:
	ModelRenderer();
	virtual ~ModelRenderer() = default;

private:
	unsigned int programId;
};