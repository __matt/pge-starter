# Welcome to the PGE Repo!

This is a place for resources and example code accompanying the Programming for Game Engines tutorials.
I'm here to help! If you have any troubles following the tutorials or getting things to work, get in touch by:

 - emailing me at [Mattia.Colombo@bcu.ac.uk](mailto:mattia.colombo@bcu.ac.uk);
 - or directly messaging me on Discord: \_\_mattia\_\_#0808
 
 ![Terrain](docs/terrain.png)
